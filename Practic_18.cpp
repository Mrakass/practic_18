﻿#include <iostream>
#include <string>
using namespace std;



class Player {
public:
	string name;
	int score;
};

void BubbleSort(Player* ArrayPlayers, int CountPlayers);

int main() {
	int count;
	cout << "Enter numbers of players: ";
	cin >> count;

	Player* players = new Player[count];

	for (int i = 0; i < count; i++) {
		cout << "Enter player name " << i + 1 << ": ";
		cin >> players[i].name;
		cout << "Score: ";
		cin >> players[i].score;
	}

	BubbleSort(players, count);

	cout << "leaderboard: " << endl;
	for (int i = 0; i < count; i++) {
		cout << players[i].name << ": " << players[i].score << endl;
	}

	delete[] players;
	return 0;
}

void BubbleSort(Player* ArrayPlayers, int CountPlayers) {
	bool b = true;
	while (b) 
	{
		b = false;
		for (int i = 0; i < CountPlayers - 1; i++) {
			if (ArrayPlayers[i].score < ArrayPlayers[i + 1].score) {
				swap(ArrayPlayers[i], ArrayPlayers[i + 1]);
				b = true;
			}
		}
	}
}



